This is a Title
===============
That has a paragraph about a main subject and is set when the '='
is at least the same length of the title itself.

Subject Subtitle
----------------
Subtitles are set with '-' and are required to have the same length 
of the subtitle itself, just like titles.

child subject
*************
i am child subject 

child child
###########

* This is a first item.
* This is a two item.
* This is a three item.

  - This is a child item.
  
    - This is a sub child.
  - This is a equal child item.
    hello world.
  - This is a son item.
    skdjfaksdjfkasjdfk.
  - This is a * .

1. This is a number list.
#. This is a # number.

a. This is 4 number.

A. This is a another # number, haha is \ *one*\ word.

j) this is j.

(K) This is K.

Lists can be unnumbered like:

 * Item Foo
 * Item Bar


   
Or automatically numbered:

 #. Item 1
 #. Item 2

*鸡*
  1. 两条腿, 直立行走, 带翅膀, 有头冠的动物.保持换行符，本行不续行,
     hahaha
  #. sssk
  #. ddkkdkd.

*鸭*
  鸡的崇拜者
 
`GitHUB`_
`fig_0601`_
 
.. _GitHub: http://github.com
 
嵌入程序代码 续 生成::

    # Dumping data for table `item_table`
    d
    d
    d
    dd
    d
    d
    d
    
    INSERT INTO item_table VALUES (
      0000000001, 0, 'Manual', '', '0.18.0',
      'This is the manual for Mantis version 0.18.0.\r\n\r\nThe Mantis manual is modeled after the [url=http://www.php.net/manual/en/]PHP Manual[/url]. It is authored via the \\"manual\\" module in Mantis CVS.  You can always view/download the latest version of this manual from [url=http://mantisbt.sourceforge.net/manual/]here[/url].',
      '', 1, 1, 20030811192655);

.. _fig_0601:
.. figure:: _static/pp.bmp

Inline Markup
-------------
Words can have *emphasis in italics* or be **bold** and you can define
code samples with back quotes, like when you talk about a command: ``sudo`` 
gives you super user powers!

.. code-block:: python
    :linenos:
    
    def foo():
        print "Love Python, Love FreeDome"
        print "E文标点,.0123456789,中文标点,. "